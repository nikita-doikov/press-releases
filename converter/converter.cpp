
/*
	This program takes dictionary and file with word indexes 
	and returns file with corresponding words.
*/


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cctype>
#include <iomanip>

using namespace std;


int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		cerr << "Usage: " << string(argv[0]) << "   dictionaryFile   inputFile   outputFile" << endl;
		return 0;
	}
	
	ifstream dictionaryFile(argv[1]);
	string buf;
	vector <string> words;
	while (dictionaryFile >> buf)
	{
		words.push_back(buf);
		if (!(dictionaryFile >> buf))
			break;
	}
	dictionaryFile.close();

	ifstream inputFile(argv[2]);
	ofstream outputFile(argv[3]);
	while (getline(inputFile, buf))
	{
		if (buf.empty() || !isdigit(buf[0]))
		{
			outputFile << buf << "\n";
		}
		int index;
		double value;
		stringstream ss(buf);
		while (ss >> index >> value)
		{
			outputFile.width(20);
			outputFile << words[index - 1] << "   ";
		}
		outputFile << "\n";
	}
	inputFile.close();
	outputFile.close();

	return 0;
}