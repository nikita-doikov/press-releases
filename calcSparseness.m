

function avgSparseness = calcSparseness( A, eps )

    avgSparseness = reshape(...
        sum(sum(A < eps, 1), 2) / (size(A, 1) * size(A, 2)), size(A, 3), 1);
    
end
