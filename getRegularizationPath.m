
%GETREGULARIZATIONPATH returns values of regularizations coefficients on
%each iteration.
%
%   Usage:
%   path = getRegularizationPath(iterationsNumber, ...
%                                sparsingStartIteration, ...
%                                lambda0, lambda1, lambda2, ...
%                                mu0, mu1, mu2, tau1, tau2)
%   
%   iterationsNumber - total iterations of algorithm.
%   sparsingStartIteration - from this iteration sparsing is started.
%
%   lambda0 - smoothing noise topics in Phi.
%   lambda1 - sparsing Phi.
%   lambda2 - decorrelating Phi.
%   
%   mu0 - smoothing noise topics in Theta.
%   mu1 - sparsing Theta.
%   mu2 - sparsing p(t).
%
%   tau1 - sparsing p(t|y)
%   tau2 - smoothing p(t|y) by y - experimental
%
%   All coefficients except tau2 must be from [0, 1].
%
%   Strategy: constant smoothing from first iteration; 
%       sparsing from sparsingStartIteration with linear increase to max value.
%

function path = getRegularizationPath(iterationsNumber, ...
                                      sparsingStartIteration, ...
                                      lambda0, lambda1, lambda2, ...
                                      mu0, mu1, mu2, tau1, tau2)

    path.lambda0 = ones(iterationsNumber, 1) * lambda0;
    path.lambda1 = getLinear(lambda1);
    path.lambda2 = getLinear(lambda2);

    path.mu0 = ones(iterationsNumber, 1) * mu0;
    path.mu1 = getLinear(mu1);
    path.mu2 = getLinear(mu2);
    
    path.tau1 = getLinear(tau1);
    path.tau2 = getLinear(tau2);
                                      
    
    function vec = getLinear(coefficient)
        vec = zeros(iterationsNumber, 1);
        for iteration = sparsingStartIteration:iterationsNumber
            vec(iteration) = (iteration - iterationsNumber) * ...
                (coefficient / (iterationsNumber - sparsingStartIteration + 1)) + ...
                coefficient;
        end    
    end
                                  
end








