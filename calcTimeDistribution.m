%CALCTIMEDISTRIBUTION computes distributions p(t|y).
%   Return value: |T| x |Y| matrix F = (p(t|y)).


function F = calcTimeDistribution(docs, timeInfo, theta)

    topicsNumber = size(theta, 1);
    Y = unique(timeInfo);
    nd = sum(docs, 2);
    ySize = size(Y, 1);
    
    F = zeros(topicsNumber, ySize);
    for y = 1:ySize
        docsMask = timeInfo == Y(y);
        ny = sum(nd(docsMask));
        F(:,y) = (theta(:,docsMask) * nd(docsMask))' / (ny);
    end
    
end