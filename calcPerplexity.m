
function perplexity = calcPerplexity(docs, phi, theta)

	EPS = 1e-9;

	n = sum(sum(docs));

	Z = phi * theta;
	indexes = Z' < EPS;

	logZ = Z';
	logZ(indexes) = 1;
	logZ = log(logZ);

	likelihood = sum(sum(docs .* logZ));
	perplexity = exp(-likelihood / n);

end