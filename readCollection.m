
%READCOLLECTION reads data from files and saves it to collection.
% 
%   Usage:
%   collection = READCOLLECTION(docsFile, jointFrequenciesFile, timeInfoFile);
%
%   docsFile - csv-file with D*W matrix, occurrences of words in documents.    
%   jointFrequenciesFile - csv-file with W*W matrics, cooccurrences of words;
%       used for coherence calculating.                       
%   timeInfoFile - D*1 column-vector of time labels for all documents.

function collection = readCollection(docsFile, ...
                                     jointFrequenciesFile, timeInfoFile)

    collection.docs = csvread(docsFile);
    collection.jointFreqs = csvread(jointFrequenciesFile);
    collection.timeInfo = csvread(timeInfoFile);

end

