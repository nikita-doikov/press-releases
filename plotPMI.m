
function fig = plotPMI(values, xTicks, titleStr, xLabel, yLabel)

    fig = figure;
    axes1 = axes('Parent',fig,'FontSize',16,'YGrid','on','XGrid','on');

    box(axes1, 'on');
    hold(axes1, 'all');
    
    if nargin >= 3
        title(titleStr, 'FontSize', 20);
    end

    k = size(values, 2);
    for i = 1:k
        plot(xTicks, values(:, i), '-s', ...
            'Color', [0.9251 0.8417 0.3574], ...
            'LineWidth', 2, 'MarkerSize', 8);
    end
    
    hold off;
    if nargin >= 4
        xlabel(xLabel);
    end
    if nargin >= 5
        ylabel(yLabel);
    end
    
end

