

if ~exist('experimentCounter', 'var')
    experimentCounter = 0;
end
experimentCounter = experimentCounter + 1;

FILE_PREFIX = './graphs/expTimeSparsing/'; % path to save graphs and models
FILE_SUFFIX = num2str(experimentCounter);


PATH_TO_DOCS_MATRIX = 'data/matrix.txt';
PATH_TO_JOINT_FREQUENCIES_MATRIX = 'data/statistics.txt';
PATH_TO_TIME_INFO = 'data/dates.txt';
PATH_TO_DICTIONARY = 'data/dictionary.txt';
PATH_TO_CONVERTER = './Press\ releases/converter/converter'; % Compile converter.cpp and specify path to the binary.


%% getting the collection

% collection = readCollection(PATH_TO_DOCS_MATRIX, ...
%                           PATH_TO_JOINT_FREQUENCIES_MATRIX, ...
%                           PATH_TO_TIME_INFO);


%% creating initialization

docsNumber = size(collection.docs, 1);
wordsNumber = size(collection.docs, 2);
topicsNumber = 100;
noiseTopicsNumber = 1;

rng(31415);
init = getRandomInit(topicsNumber, docsNumber, wordsNumber);


%% creating regularization path

iterationsNumber = 100;
sparsingStartIteration = 20;

lambda0 = 0; % smoothing noise topics in Phi
lambda1 = 0.1; % sparsing Phi
lambda2 = 0; % decorrelating Phi - DO NOT USED
mu0 = 0.5; % smoothing noise topics in Theta
mu1 = 0.05; % sparsing Theta
mu2 = 0; % sparsing p(t) - DO NOT USED
tau1 = 0.005; % sparsing p(t|y)
tau2 = 0; % smoothing p(t|y) by y      

regularizationPath = getRegularizationPath(iterationsNumber, ...
                                           sparsingStartIteration, ...
                                           lambda0, lambda1, lambda2, ...
                                           mu0, mu1, mu2, tau1, tau2);
            

%% launch EM-algorithm

disp('launch EM');

tic;
models = EM(collection, init, topicsNumber, noiseTopicsNumber, ...
            regularizationPath, iterationsNumber);
t = toc;
disp(['Time: ', num2str(t)]);



%% save the models
%save([FILE_PREFIX, 'models', FILE_SUFFIX, '.mat'], ...
%    'models', 'init', 'noiseTopicsNumber', 'regularizationPath', 'iterationsNumber');

        
        
%% Get and plot metrics

disp('Calculating metrics');

tic;
metrics = calcMetrics(collection, models);
t = toc;
disp(['Time: ', num2str(t)]);
save([FILE_PREFIX, 'metrics', FILE_SUFFIX, '.mat'], 'metrics');

fig = plotMetrics(metrics, 1:iterationsNumber, 'Metrics', ...
              'iterations', 'values of metrics');        
saveFigure(fig, [FILE_PREFIX, 'metrics', FILE_SUFFIX]);

fig = plotPMI(metrics.PMI, 1:iterationsNumber, 'Coherence', ...
              'iterations', 'value of coherence');
saveFigure(fig, [FILE_PREFIX, 'coherence', FILE_SUFFIX]);


%% Display topics
WORDS_PER_TOPIC = 10;
TOPICS_MASK = 1:(topicsNumber - noiseTopicsNumber);

F = calcTimeDistribution(collection.docs, collection.timeInfo, models.theta(:,:,end));
fig = plotTopicsInTime(F, TOPICS_MASK, 0.5, 1);
saveFigure(fig, [FILE_PREFIX, 'time', FILE_SUFFIX]); 

writeTopicWords(models.phi(:,:,end), WORDS_PER_TOPIC, TOPICS_MASK, FILE_PREFIX, ...
    ['topics', FILE_SUFFIX], ...
    [PATH_TO_CONVERTER, ' ', PATH_TO_DICTIONARY]);
    

