
%SAVEFIGURE(fig, filename) saves figure into files filename.png and filename.eps

function saveFigure(fig, filename)

    print(fig, '-depsc', [filename, '.eps']);
    print(fig, '-dpng', [filename, '.png']); 

end

