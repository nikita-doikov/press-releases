%EM-algorithm for PLSA topic model with Additive Regularization.
%
%   Usage:
%   models = EM(collection, init, topicsNumber, noiseTopicsNumber, ...
%               path, iterationsNumber)
%
%   collection - input data structure returned by readCollection().
%   
%   init: init.phi, init.theta - initialization values of matrices. 
%   
%   topicsNumber - number of Topics in model.
%   noiseTopicsNumber - number of Topics for Stop words.
%   
%   path - set of regularization coefficients for each iteration i:       
%       path.lambda0(i) - smoothing noise topics in Phi; 
%       path.lambda1(i) - sparsing Phi;
%       path.mu0(i) - smoothing noise topics in Theta;
%       path.mu1(i) - sparsing Theta;
%       path.tau1(i) - sparsing p(t|y);
%       path.tau2(i) - smoothing p(t|y) by y;
%
%       All coefficients except tau2 must be from [0, 1].
%       Coefficients could be taken from getRegularizationPath().
%
%   iterationsNumber - number of iterations for EM-algorithm.
%
%   Return value: models - matrices phi and theta after each iteration:
%       models.phi(:,:,i) - matrix Phi after iteration i;
%       models.theta(:,:,i) - matrix Theta after iteration i.


function models = EM(collection, init, topicsNumber, noiseTopicsNumber, ...
                     path, iterationsNumber)
%% initial calculations                 
    EPS = 1e-9;
    
    docsNumber = size(collection.docs, 1);
    wordsNumber = size(collection.docs, 2);

    nd = sum(collection.docs, 2); % lengths of documents
    nw = sum(collection.docs, 1)'; % counting occurences of words
    n = sum(nw); % total size of collection
    beta = nw / n; % distribution of words in dictionary
    
    normalTopicsNumber = topicsNumber - noiseTopicsNumber;
    normalTopicsMask = 1:topicsNumber <= normalTopicsNumber;
    noiseTopicsMask = ~normalTopicsMask;

    phi = init.phi;
    theta = init.theta;
    
    models.phi = zeros(wordsNumber, topicsNumber, iterationsNumber);
    models.theta = zeros(topicsNumber, docsNumber, iterationsNumber);
    
    Y = unique(collection.timeInfo);
    ySize = size(Y, 1);
    F = zeros(topicsNumber, ySize);
    ny = zeros(ySize, 1);
    for y = 1:ySize
        docsMask = collection.timeInfo == Y(y);
        ny(y) = sum(nd(docsMask));
        F(:,y) = (theta(:,docsMask) * nd(docsMask))' / (ny(y));
    end
    
    
    for iteration = 1:iterationsNumber
    %% E-step    
        Z = collection.docs ./ (phi * theta)';
        nwt = phi .* (theta * Z)';
        ntd = theta .* (Z * phi)';
        nt = sum(ntd, 2);
        
    %% M-step
        phi = nwt;
        theta = ntd;
        
        
        % calculating regularization coefficients
        beta0 = nt(noiseTopicsMask)' * ...
            path.lambda0(iteration) / (1 - path.lambda0(iteration));
        alpha0 = nd' * path.mu0(iteration) / (1 - path.mu0(iteration));
        beta1 = repmat(path.lambda1(iteration), 1, normalTopicsNumber) .* ...
            max(nwt(:, normalTopicsMask) ./ ...
                repmat(beta, 1, normalTopicsNumber), [], 1);
        alpha1 = repmat(path.mu1(iteration), 1, docsNumber) .* ...
            max(topicsNumber * ntd(normalTopicsMask, :), [], 1);
        
        
        % calculating time regularizers
        theta_nd = theta(normalTopicsMask, :) .* ...
            repmat(nd', normalTopicsNumber, 1);
        rtau1 = zeros(normalTopicsNumber, docsNumber); % sparsing p(t|y) 
        rtau2 = zeros(topicsNumber, docsNumber); % smoothing p(t|y) by y
        for d = 1:docsNumber
            
            y = collection.timeInfo(d);
            docsMask = collection.timeInfo == y;
            yIndex = find(Y == y);
            
            A = sum(theta_nd(:, docsMask), 2);
            tau1 = path.tau1(iteration) * ...
                max(topicsNumber * ntd(normalTopicsMask, d) .* A ./ ...
                    (nd(d) * theta(normalTopicsMask, d)));
            rtau1(:, d) = (-1 / topicsNumber) * nd(d) * tau1 * ...
                theta(normalTopicsMask, d) ./ A;
            
            B = zeros(topicsNumber, 1);
            if yIndex > 1
                B = B + signEps(F(:, yIndex - 1) - F(:, yIndex), EPS);
            end
            if yIndex < ySize
                B = B + signEps(F(:, yIndex + 1) - F(:, yIndex), EPS);
            end
            vec = (nd(d) / ny(yIndex)) * (theta(:,d) .* B);
            rtau2(:,d) = vec * path.tau2(iteration) * max(-ntd(:,d) ./ vec);
        end
        
            
        % smoothing noise topics
        phi(:, noiseTopicsMask) = phi(:, noiseTopicsMask) + ...
            repmat(beta0, wordsNumber, 1) .* ...
            repmat(beta, 1, noiseTopicsNumber);
        theta(noiseTopicsMask, :) = theta(noiseTopicsMask) + ...
            repmat(alpha0, noiseTopicsNumber, 1) * (1 / topicsNumber);
        
        
        % sparsing normal topics
        phi(:, normalTopicsMask) = phi(:, normalTopicsMask) - ...
            repmat(beta1, wordsNumber, 1) .* ...
            repmat(beta, 1, normalTopicsNumber);
        theta(normalTopicsMask, :) = theta(normalTopicsMask, :) - ...
            repmat(alpha1, normalTopicsNumber, 1) * (1 / topicsNumber);
        
        % time regularization
        theta(normalTopicsMask, :) = theta(normalTopicsMask, :) + rtau1; 
        theta = theta + rtau2;
        
        
        phi = max(phi, EPS);
        theta = max(theta, EPS);
        
        phi = phi ./ repmat(sum(phi, 1), wordsNumber, 1);
        theta = theta ./ repmat(sum(theta, 1), topicsNumber, 1);
    
        models.phi(:,:,iteration) = phi;
        models.theta(:,:,iteration) = theta;
        
        % calculate Time Distribution
        for y = 1:ySize
            docsMask = collection.timeInfo == Y(y);
            F(:,y) = (theta(:,docsMask) * nd(docsMask))' / (ny(y));
        end
    end

    
    function y = signEps(x, eps)
        y = zeros(size(x));
        y(x > eps) = 1;
        y(x < eps) = -1;
    end
    
    
end

