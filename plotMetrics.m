
function fig = plotMetrics(metrics, xTicks, titleStr, xLabel, yLabel)

    fig = figure;
    axes1 = axes('Parent',fig,'FontSize',16,'YGrid','on','XGrid','on');

    ylim(axes1, [0, 1]);
    box(axes1, 'on');
    hold(axes1, 'all');
    
    if nargin >= 3
        title(titleStr, 'FontSize', 20);
    end

    COLORS = [
        0.8545 0.3000 0.3000;
        0.3173 0.4148 0.8663;
        0.5018 0.6240 0.1156;
        0.4264 0.8934 0.9442;
        0.9251 0.8417 0.3574;
        0.5725 0.0957 0.8853;
        0.9808 0.6848 0.4809;
        0.5316 0.5318 0.6344;
        0.3428 0.3041 0.4170;
        0.7050 0.9954 0.3599;
    ];
    
    plot(xTicks, metrics.perplexity / 1000, 'Color', COLORS(1,:), 'LineWidth', 3);
    plot(xTicks, metrics.phiSparseness, '--', 'Color', COLORS(2,:), 'LineWidth', 3);
    plot(xTicks, metrics.thetaSparseness, '--', 'Color', COLORS(3,:), 'LineWidth', 3);
    plot(xTicks, metrics.fSparseness, '--', 'Color', COLORS(10,:), 'LineWidth', 5);
    plot(xTicks, metrics.purity, '-o', 'Color', COLORS(6,:), 'LineWidth', 2);
    plot(xTicks, metrics.contrast, '-*', 'Color', COLORS(7,:), 'LineWidth', 2);
    plot(xTicks, metrics.noisiness, '-v', 'Color', COLORS(8,:), 'LineWidth', 2);
    plot(xTicks, metrics.RDP, '-.', 'Color', COLORS(9,:), 'LineWidth', 3);
    plot(xTicks, metrics.variation, 'Color', COLORS(4,:), 'LineWidth', 5);
    
    legend('Perplexity', 'Phi sparseness', 'Theta sparseness', ...
            'p(t|y) sparseness', 'putiry', 'contrast', 'noisiness', 'RDP', ...
            'variation', 'Location', 'NorthEastOutside');
    
    hold off;
    if nargin >= 4
        xlabel(xLabel);
    end
    if nargin >= 5
        ylabel(yLabel);
    end
    
end

