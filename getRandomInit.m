
%GETRANDOMINIT returns random initialization of matrices Phi and Theta.

function init = getRandomInit(topicsNumber, docsNumber, wordsNumber)

	theta = rand(topicsNumber, docsNumber);
	theta = theta ./ repmat(sum(theta, 1), topicsNumber, 1);

	phi = rand(wordsNumber, topicsNumber);
	phi = phi ./ repmat(sum(phi, 1), wordsNumber, 1);


	init.theta = theta;
	init.phi = phi;

end
