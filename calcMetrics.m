
%CALCMETRICS computes a set of metrics for each model.


function metrics = calcMetrics(collection, models)

    EPS = 1e-9;    
    
    modelsNumber = size(models.phi, 3);
    nw = sum(collection.docs, 1)';
    n = sum(nw);
    beta = nw / n;
    normBeta = sum(beta .^ 2);
    topicsNumber = size(models.theta, 1);
    
    
    metrics.phiSparseness = calcSparseness(models.phi, EPS);
    metrics.thetaSparseness = calcSparseness(models.theta, EPS);
    
    metrics.noisiness = reshape(...
            median(sum(models.phi .* ...
                repmat(beta, [1, topicsNumber, modelsNumber]), 1)), ...
        modelsNumber, 1) / normBeta;
    
    
    THRESHOLD = 0.05;
    metrics.determinedTopicsNumber = reshape(...
        sum(sum(models.theta, 2) > THRESHOLD, 1), modelsNumber, 1);
    
    KERNEL_THRESHOLD = 0.25;
    
    metrics.fSparseness = zeros(modelsNumber, 1);
    metrics.variation = zeros(modelsNumber, 1);
    metrics.perplexity = zeros(modelsNumber, 1);
    metrics.PMI = zeros(modelsNumber, 1);
    metrics.RDP = zeros(modelsNumber, 1);
    metrics.purity = zeros(modelsNumber, 1); 
    metrics.contrast = zeros(modelsNumber, 1);
    
    
    for i = 1:modelsNumber
        
        F = calcTimeDistribution(collection.docs, collection.timeInfo, ...
                models.theta(:,:,i));
        metrics.fSparseness(i) = calcSparseness(F, EPS);
        metrics.perplexity(i) = calcPerplexity(collection.docs, ...
            models.phi(:,:,i), models.theta(:,:,i));
        
        
        phiAscend = sort(models.phi(:,:,i));
        phiDescend = sort(models.phi(:,:,i), 'descend');
        
        dotProducts = models.phi(:,:,i)' * models.phi(:,:,i);
        minDotProducts = phiAscend' * phiDescend;
        maxDotProducts = phiAscend' * phiAscend;
        rescaledDotProducts = (dotProducts - minDotProducts) ./ ...
            (maxDotProducts - minDotProducts);
        sz = size(rescaledDotProducts, 1) * size(rescaledDotProducts, 2);
        metrics.RDP(i) = ...
            median(reshape(rescaledDotProducts, sz, 1));
        
        Z = collection.docs ./ (models.phi(:,:,i) * models.theta(:,:,i))';
        ntd = models.theta(:,:,i) .* (Z * models.phi(:,:,i))';
        nt = sum(ntd, 2);
        
        ptw = models.phi(:,:,i) .* ((1 ./ nw) * nt');
        kernel = ptw > KERNEL_THRESHOLD;
        
        purity = zeros(topicsNumber, 1);
        contrast = zeros(topicsNumber, 1);
        for t = 1:topicsNumber
            purity(t) = sum(models.phi(kernel(:,t), t, i));
            if sum(kernel(:,t)) > 0
                contrast(t) = sum(ptw(kernel(:,t), t)) / sum(kernel(:,t));
            end
        end
        metrics.purity(i) = median(purity);
        metrics.contrast(i) = median(contrast);
        
        metrics.PMI(i) = calcPMI(models.phi(:,:,i), ...
            collection.jointFreqs, EPS);
        
        F = sqrt(F);
        metrics.variation(i) = median(...
            sum(abs(F - [F(:,1), F(:,1:end - 1)]), 2)); 
    end
    
end

