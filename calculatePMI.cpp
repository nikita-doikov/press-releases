
#include "mex.h"
#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Declare variables */
    size_t K;
    size_t topicsNumber;
    size_t wordsNumber;

    double *indexes;
    double *N;
    double eps;
    double *coherence;
    double coherenceValue;
    double totalN;
    std::vector <double> coherenceValues;
    int med;

    /* Check for proper number of input and output arguments */
    if (nrhs != 3) 
        mexErrMsgTxt("Three input arguments required.");
    
    if (nlhs > 1) 
        mexErrMsgTxt("Too many output arguments.");
    
    
    K = mxGetM(prhs[0]);
    topicsNumber = mxGetN(prhs[0]);
    wordsNumber = mxGetM(prhs[1]);
    
    indexes = mxGetPr(prhs[0]);
    N = mxGetPr(prhs[1]);
    eps = *mxGetPr(prhs[2]);

    totalN = 0;
    for (int i = 0; i < wordsNumber; ++i)
    {
        totalN += N[i * wordsNumber + i];
    }
    
    for (int topic = 0; topic < topicsNumber; ++topic)
    {
        coherenceValue = 0;
    	for (int i = 1; i < K; ++i)
    	{
    		for (int j = 0; j < i; ++j)
    		{
    			int indexI = (int)indexes[topic * K + i] - 1;
    			int indexJ = (int)indexes[topic * K + j] - 1;
    			coherenceValue += log( (N[indexI * wordsNumber + indexJ] * totalN + eps) 
                    / (N[indexJ * wordsNumber + indexJ] * N[indexI * wordsNumber + indexI]) );
    		} 
    	}
        coherenceValues.push_back(coherenceValue / (0.5 * K * (K - 1)));
    }
    
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
    coherence = mxGetPr(plhs[0]);
    med = coherenceValues.size() / 2;
    std::nth_element(coherenceValues.begin(), 
            coherenceValues.begin() + med, coherenceValues.end());
    *coherence = coherenceValues[med];
}