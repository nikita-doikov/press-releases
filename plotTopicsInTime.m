
function fig = plotTopicsInTime(F, topicsMask, normalizeDeg, gaussSigma)

    if nargin >= 2
        F = F(topicsMask, :);
    end
    
    if nargin >= 3
        total = sum(F, 1);
        total = total .^ normalizeDeg;
        F = F ./ repmat(total, size(F, 1), 1);
    end
    if nargin >= 4
        G = fspecial('gaussian', ...
            [1, max(1, ceil(3 * gaussSigma))], gaussSigma);
        for i = 1:size(F, 1)
            F(i,:) = imfilter(F(i,:), G, 'same');
        end
    end

    fig = figure;
    axes1 = axes('Parent', fig);
    xlim(axes1, [1 size(F, 2)]);
    
    box(axes1, 'on');
    hold(axes1, 'all');
    area(F', 'Parent', axes1);

end

