% CALCPMI computes median PMI-score.
%
%   Compile calculatePMI.cpp to mex-file before using.
%   Example: 
%   mex calculatePMI.cpp

function coherence = calcPMI(phi, N, eps)
    
    [~,IX] = sort(phi, 'descend');
    K = 10;
    
    coherence = calculatePMI(IX(1:K,:), N, eps);

end
