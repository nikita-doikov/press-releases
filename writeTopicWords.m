function writeTopicWords(phi, wordsPerTopic, topicsMask, pathToFiles, ...
                         fileName, converterScript)
    
                     
    fid = fopen([pathToFiles, 'tmp_', fileName], 'wt');

    phi = phi(:, topicsMask, :);
    [~,IX] = sort(phi, 'descend');
    topicsNumber = size(phi, 2);
    modelsNumber = size(phi, 3);
    for t = 1:topicsNumber
        
        fprintf(fid, 'Topic #%d\n', topicsMask(t));
        model = 1;
        while model <= modelsNumber
            for k = 1:wordsPerTopic
                for i = 0:2
                    index = IX(k, t, model + i);
                    phiValue = phi(index, t, model + i);
                    fprintf(fid, '%d %f   ', index, phiValue);
                    if model + i + 1 > modelsNumber
                        break;
                    end
                end
                fprintf(fid, '\n');
            end
            model = model + 3;
            fprintf(fid, '\n');
        end
        
    end
    
    fclose(fid);

    system([converterScript, ' ', pathToFiles, 'tmp_', fileName, ' ', pathToFiles, fileName]);
    

end

